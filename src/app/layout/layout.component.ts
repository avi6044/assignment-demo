import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    constructor(@Inject(DOCUMENT) private document: Document,private element: ElementRef){}
    ngOnInit(): void {

    }

    @HostListener('window:scroll', [])
    onWindowScroll() {
     var navbar: HTMLElement =this.element.nativeElement.children[1].children[0] ;
     var button: HTMLElement =this.element.nativeElement.children[5] ;
     var sticky = navbar.offsetTop;
      if (document.body.scrollTop > sticky || document.documentElement.scrollTop > sticky) {
          button.style.display = "block";
          navbar.classList.add('sticky');
          button.classList.add("lightSpeedIn");
          button.classList.remove("lightSpeedOut");
      } else  {
          navbar.classList.remove('sticky');
          button.classList.remove("lightSpeedIn");
          button.classList.add("lightSpeedOut");
      }
    }

    backToTop () {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
}
