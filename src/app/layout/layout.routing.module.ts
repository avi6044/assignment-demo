import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../components/home/home.component";
import { LAYOUT_ROUTES_PATH } from "./layout-routes";
import { LayoutComponent } from "./layout.component";

const routes:Routes = [
    {
        path:LAYOUT_ROUTES_PATH.LAYOUT_PATH,
        component:LayoutComponent,
        children: [
            {
                path:LAYOUT_ROUTES_PATH.LAYOUT_HOME,
                component:HomeComponent
            },
            {
                path:'',
                redirectTo:LAYOUT_ROUTES_PATH.LAYOUT_HOME,
                pathMatch:'full'
            }
        ]
    }

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}