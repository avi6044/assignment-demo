import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as sharedComponents from '.';

const components = [
   sharedComponents.FooterComponent,
   sharedComponents.NavbarComponent,
   sharedComponents.ToolBarComponent
]

@NgModule({
  declarations: [
    ...components
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
      ...components
  ],
  providers: [],
})
export class SharedModule { }