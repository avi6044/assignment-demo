import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  slideConfig = {
    dots: false,
    infinite: true,
    speed: 500,
    arrows: false,
    fade: true,
    slidesToShow: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    cssEase: 'linear',
    adaptiveHeight: true
   
  };

  featureArray: Array<any> = [
    {
      label:"Business",
      isActive:true
    },
    {
      label:"Completion",
      isActive:false
    },{
      label:"Criminal Justice",
      isActive:false
    },{
      label:"Education",
      isActive:false
    },{
      label:"Humanities & Social Sciences",
      isActive:false
    },{
      label:"Public Service",
      isActive:false
    },{
      label:"Science",
      isActive:false
    }
  ];
  currentSelectedFeature: string =  "Business";

  requestForm =  new FormGroup({
    'firstName': new FormControl('',[Validators.required]),
    'lastName': new FormControl('',[Validators.required]),
    'email': new FormControl('',[Validators.required,Validators.email]),
    'phoneNumber': new FormControl('',[Validators.required]),
    'interest': new FormControl('',[Validators.required]),
    'deliveryType': new FormControl('',[Validators.required]),
    'communicationType': new FormControl('',[Validators.required]),
  })

  constructor() {
   }

  ngOnInit(): void {
  }


  changeFeature(index:number) {
    this.featureArray.map((item,i) => {
        if(i === index){
          item.isActive = true;
          this.currentSelectedFeature = item.label;
        } else {
          item.isActive = false;
        }
    })
  }

}
